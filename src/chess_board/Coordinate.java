package chess_board;

/**
 * Represents a position on the chess board
 * <p>Used in the Space class
 * @author Group 35: Jarisha Olanday (jmo185) & Leo Scarano (ls949)
 */
public class Coordinate {

    /**
     * Value for rank or row
     * Directly corresponds to board value
     */
    private int rank;
    /**
     * Value for file or column
     * Directly corresponds to board value
     */
    private int file;

    /**
     * Enum values represent string value of corresponding Column or file
     */
    enum Column {
        inv,a,b,c,d,e,f,g,h;

        public static Column[] COLUMNS = new Column[] {inv,a,b,c,d,e,f,g,h};
    }

    /**
     * Creates a new Coordinate with given file and rank as ints
     * @param file  file or column value
     * @param rank  rank or row value
     */
    public Coordinate(int file, int rank){
        this.rank = rank;
        this.file = file;
    }

    /**
     * Creates a new Coordinate with the given string position (i.e "e5")
     * @param position  String of board position
     */
    public Coordinate(String position){
        this.file = Column.valueOf(position.substring(0,1)).ordinal();
        this.rank = Integer.parseInt(position.substring(1));
    }

    /**
     * Returns rank field
     * @return  rank field
     */
    public int getRank(){
        return rank;
    }

    /**
     * Returns file field
     * @return  file field
     */
    public int getFile(){
        return file;
    }

    /**
     * Used to compare equivalency to other objects, primarily Coordinates
     * @param o Object to compare to
     * @return  true if it is equal, false it is not
     */
    public boolean equals(Object o){
        if(o == null || !(o instanceof Coordinate)){
            return false;
        }
        Coordinate other = (Coordinate) o;
        return this.file == other.getFile() && this.rank == other.getRank();
    }

    /**
     * returns String representation of Coordinate (i.e "e5", "d4")
     * @return
     */
    public String toString(){
        return Column.COLUMNS[this.file] + "" + this.rank;
    }

}
