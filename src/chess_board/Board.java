package chess_board;
import chess_pieces.*;
public class Board {
    public Space[][] board;
    public static Space whiteKingCoord;
    public static Space blackKingCoord;

    /* Example board:

    bR bN bB bQ bK bB bN bR 8
    bp bp bp bp bp bp bp bp 7
       ##    ##    ##    ## 6
    ##    ##    ##    ##    5
       ##    ##    ##    ## 4
    ##    ##    ##    ##    3
    wp wp wp wp wp wp wp wp 2
    wR wN wB wQ wK wB wN wR 1
     a  b  c  d  e  f  g  h

     */

    /* Creates a chess board for a new game like the example
     * Instanstiates the board member with Spaces
     * Will fill in each row of the chess board from top to bottom, left to right
     * Top board of ascii art will be the last row of the 2d array
     */
    public Board(){
        board = new Space[8][8];
        //rank 8, black non-pawn pieces
        board[7][0] = new Space(true, new Rook(false), new Coordinate("a8"));
        board[7][1] = new Space(true, new Knight(false), new Coordinate("b8"));
        board[7][2] = new Space(true,new Bishop(false), new Coordinate("c8"));
        board[7][3] = new Space(true,new Queen(false), new Coordinate("d8"));
        board[7][4] = new Space(true,new King(false), new Coordinate("e8"));
        blackKingCoord = board[7][4];
        board[7][5] = new Space( false, new Bishop(false), new Coordinate("f8"));
        board[7][6] = new Space( true, new Knight(false), new Coordinate("g8"));
        board[7][7] = new Space( false, new Rook(false), new Coordinate("h8"));
        //rank 7, black pawns
        board[6][0] = new Space(false, new Pawn(false),new Coordinate("a7"));
        board[6][1] = new Space(true, new Pawn(false), new Coordinate("b7"));
        board[6][2] = new Space(false, new Pawn(false), new Coordinate("c7"));
        board[6][3] = new Space(true, new Pawn(false), new Coordinate("d7"));
        board[6][4] = new Space(false, new Pawn(false), new Coordinate("e7"));
        board[6][5] = new Space(true, new Pawn(false), new Coordinate("f7"));
        board[6][6] = new Space(false, new Pawn(false), new Coordinate("g7"));
        board[6][7] = new Space(true, new Pawn(false), new Coordinate("h7"));

        //fill in row 2 - 5 with empty spaces
        for(int r = 5; r >= 2; r--){
            for(int c = 0; c <=7; c++){
                if(r % 2 == 0){
                    if(c % 2 == 0)
                        board[r][c] = new Space(false,new Coordinate(c+1,r+1));
                    else
                        board[r][c] = new Space(true, new Coordinate (c+1,r+1));
                }else{
                    if(c % 2 == 0)
                        board[r][c] = new Space(true, new Coordinate(c+1,r+1));
                    else
                        board[r][c] = new Space(false, new Coordinate(c+1 ,r+1));
                }
            }
        }

        //rank 2, white pawns
        board[1][0] = new Space(true, new Pawn(true), new Coordinate("a2"));
        board[1][1] = new Space(false, new Pawn(true), new Coordinate("b2"));
        board[1][2] = new Space(true, new Pawn(true), new Coordinate("c2"));
        board[1][3] = new Space(false, new Pawn(true), new Coordinate("d2"));
        board[1][4] = new Space(true, new Pawn(true), new Coordinate("e2"));
        board[1][5] = new Space(false, new Pawn(true), new Coordinate("f2"));
        board[1][6] = new Space(true, new Pawn(true), new Coordinate("g2"));
        board[1][7] = new Space(false, new Pawn(true),new Coordinate("h2"));

        //rank 1, white non-pawn pieces
        board[0][0] = new Space(false,new Rook(true), new Coordinate("a1"));
        board[0][1] = new Space(true, new Knight(true), new Coordinate("b1"));
        board[0][2] = new Space(false, new Bishop(true), new Coordinate("c1"));
        board[0][3] = new Space(true,new Queen(true), new Coordinate("d1"));
        board[0][4] = new Space( false, new King(true), new Coordinate("e1"));
        whiteKingCoord = board[0][4];
        board[0][5] = new Space( true, new Bishop(true), new Coordinate("f1"));
        board[0][6] = new Space( false, new Knight(true),new Coordinate("g1"));
        board[0][7] = new Space( true, new Rook(true), new Coordinate("h1"));


    }

    public Piece getPiece(Coordinate c){
        return board[c.getRank() - 1][c.getFile() - 1].getPiece();
    }

    public Space getSpace(Coordinate c){
        return board[c.getRank() - 1][c.getFile() - 1];
    }

    /*
     * Updates the board space after a piece is moved
     * Assumes that the move is valid
     * Assumes the piece's position is updated prior to calling this function (subject to change)
     * TODO: make a case for en passant
     */
    public void movePiece(Coordinate src, Coordinate dest){
        //Get copy of piece at the source space
        Piece p_src = board[src.getRank() - 1][src.getFile() - 1].getPiece();

        //Get copy of destination space
        Space s_dst = board[dest.getRank() - 1][dest.getFile() - 1];

        //if the destination is the same as the source, do nothing (should never be the case)
        if(src.equals(dest)){
            return;
        }

        //Else if source & destination are different, move the piece off the source space
        board[src.getRank() - 1][src.getFile() - 1].removePiece();

        //Then move the piece to the destination
        //If destination space is not empty, remove piece at destination (capture)
        if(!s_dst.isEmpty){
             board[dest.getRank() - 1][dest.getFile() - 1].removePiece();
        }
        if(p_src instanceof Pawn){
            ((Pawn) p_src).updateStep(src,dest);
        }

        //add src piece to destination
        board[dest.getRank() - 1][dest.getFile() - 1].addPiece(p_src);


    }

    /*
     * Does the same thing as the movePiece method above but accepts String arguments
     * Assumes arguments are of valid format and are valid moves (i.e "e4", "d6", etc)
     */

    public void movePiece(String src, String dest){
        movePiece(new Coordinate(src),new Coordinate(dest));

    }

    public void undoMove(Coordinate currSrc, Coordinate prevSrc, Space oldDestSpace){
        Piece curr_piece = getSpace(currSrc).getPiece();
        movePiece(currSrc,prevSrc);
        if(!oldDestSpace.isEmpty){
            board[currSrc.getRank() - 1][currSrc.getFile() - 1].addPiece(oldDestSpace.getPiece());
        }
        if(curr_piece instanceof Pawn && ((Pawn) curr_piece).getDoubleStep()){
            ((Pawn) curr_piece).setDoubleStep(false);
        }

    }

    /**
     * Promotes the piece at the given coordinate to the specified type.
     * If the type is null, will promote to a Queen.
     * Assumes space has a Pawn.
     */
    public void promote(Coordinate c,boolean iswhite, String type){
        //First remove the pawn on the space;
        board[c.getRank() - 1][c.getFile() - 1].removePiece();

        //Then replace with specified type
        if(type == null || type.equals("Q")){
            board[c.getRank() - 1][c.getFile() - 1].addPiece(new Queen(iswhite));
        }else {
            switch (type) {
                case "R":
                    board[c.getRank() - 1][c.getFile() - 1].addPiece(new Rook(iswhite));
                    break;
                case "N":
                    board[c.getRank() - 1][c.getFile() - 1].addPiece(new Knight(iswhite));
                    break;
                case "B":
                    board[c.getRank() - 1][c.getFile() - 1].addPiece(new Bishop(iswhite));
                    break;
                default:
                    board[c.getRank() - 1][c.getFile() - 1].addPiece(new Queen(iswhite));
                    break;
            }
        }
    }



    // Prints the board with ascii art
    public void print(){
        System.out.println();
        for(int r = 7; r >= 0; r--){
            for(int c = 0; c <= 8; c++){
                if(c == 8){
                    System.out.println("" + (r + 1));
                }else{
                    System.out.print(board[r][c].toString() + " ");
                }
            }
        }

        System.out.println(" a  b  c  d  e  f  g  h ");
        System.out.println();
    }


}
