package chess_board;


import chess_pieces.Piece;

/**
 * Represents a space on a chessboard. Encapsulates all necessary information to generate a chessboard space
 * including the whether it is empty or not, color, position, and any Piece object it may have.
 *  @author Group 35: Jarisha Olanday (jmo185) & Leo Scarano (ls949)
 */
public class Space {
    /**
     * Boolean for whether the Space contains a chess piece (Piece).
     * <p>Is true if it does contain a piece, false otherwise
     */
    public boolean isEmpty;

    /**
     * Color of the space primarily used to print the ascii art of the chess board
     * <p>Is true if the space is white, false if it is black
     */
    public boolean isWhite;

    /**
     * Field for a piece the space may contain
     * <p>Is null if the space is empty
     */
    private Piece piece;

    /**
     * Position of the Space on the board (i.e "e6" or "d4"
     */
    private Coordinate position;

    /**
     * Creates a non-empty Space with a Piece with the given color and position
     * @param iswhite   Value to set the isWhite field
     * @param piece     Piece to set the piece field
     * @param position  Coordinate to set the position field
     */
    public Space(boolean iswhite, Piece piece, Coordinate position){
        this.isEmpty = false;
        this.isWhite = iswhite;
        this.piece = piece;
        this.position = position;
    }

    /**
     * Creates an empty Space. Initializes the piece field to null because it is empty.
     * @param iswhite   Value to set the isWhite field
     * @param position  Coordinate to set the position field
     */
    public Space(boolean iswhite, Coordinate position){
        this.isEmpty = true;
        this.isWhite = iswhite;
        this.piece = null;
        this.position = position;
    }

    /**
     * Returns the piece field of the Space regardless if the isEmpty field is true
     * @return  Space's piece field
     */
    public Piece getPiece(){
        return piece;
    }

    /**
     * Returns the position field of the Space
     * @return  Space's position field
     */
    public Coordinate getPosition() {
        return this.position;
    }


    /**
     * Represents removing the current piece on the Space as if to move it to another space or capturing it
     * <p>Assumes the Space has a piece
     */
    public void removePiece(){
        isEmpty = true;
        piece = null;
    }

    /**
     * Represents moving a new piece on the Space, effectively replacing the current piece if it contains one
     * @param p different Piece to move onto the Space
     */
    public void addPiece(Piece p){
        isEmpty = false;
        piece = p;
    }

    /**
     * Returns "  " if the Space is blank and white
     * <p> Returns "##" if the Space is blank and black
     * <p> Returns the Piece toString if the Space is not empty
     * @return  String representation of Space
     */
    public String toString(){
        if(isEmpty){
            if(isWhite){
                return "  ";
            }else{
                return "##";
            }
        }else{
            return piece.toString();
        }
    }


}
