package chess_pieces;
import chess_board.Coordinate;
import chess_board.Space;

import java.util.ArrayList;

import static java.lang.Math.abs;

/**
 * Represents a rook chess piece. There are 2 rooks per player in the game of chess
 *  @author Group 35: Jarisha Olanday (jmo185) & Leo Scarano (ls949)
 */
public class Rook extends Piece{

    /**
     * Creates a new Rook with the specified color using super constructor
     * @param iswhite boolean for whether or not the piece is white or black (true if white, false if black)
     */
    public Rook(boolean iswhite){
        super(iswhite);
    }
    /**
     * Implements abstract method of the same name in Piece according to the move set of a rook
     *  <p>CASES: Cases where rook move is valid
     *  <ul>
     *  <li> CASE 1: Rook moves ONLY horizontally or ONLY vertically, never both, by one space
     *  <li> CASE 2: Rook moves ONLY horizontally or ONLY vertically, never both, by more than one space
     *  </ul>
     *
     * @param src   Source space from which the piece will move
     * @return  ArrayList of Coordinates of all possible paths for a rook at the source
     */
    public ArrayList<Coordinate> returnCoordinatePath(Space src){
        ArrayList<Coordinate> path = new ArrayList<>();
        int file = src.getPosition().getFile();
        int rank = src.getPosition().getRank();

        // Increase vertically
        for(int i = 1; i + rank <= 8; ++i){
            path.add(new Coordinate(file, rank+i));
        }
        // Increase horizontally
        for(int j = 1; j + file <= 8; ++j){
            path.add(new Coordinate(file+j, rank));
        }
        // decrease vertically
        for(int i = 1; rank - i >= 1; ++i){
            path.add(new Coordinate(file, rank-i));
        }
        // decrease horizontally
        for(int j = 1; file - j >= 1; ++j){
            path.add(new Coordinate(file-j, rank));
        }

        return path;
    }

    /**
     * Implements toString override in Piece where type is represented as "R"
     * @return  "wR" for white rook or "bR" for black rook.
     */
    public String toString(){
        if(isWhite()){
            return "wR";
        }else{
            return "bR";
        }
    }
}
