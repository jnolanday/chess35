package chess_pieces;
import chess_board.Coordinate;
import chess_board.Space;

import java.util.ArrayList;


import static java.lang.Math.abs;
/**
 * Represents a bishop chess piece. There are 2 bishops per player in the game of chess
 *  @author Group 35: Jarisha Olanday (jmo185) & Leo Scarano (ls949)
 */
public class Bishop extends Piece {

    /**
     * Creates a new Bishop with the specified color using super constructor
     * @param iswhite boolean for whether or not the piece is white or black (true if white, false if black)
     */
    public Bishop(boolean iswhite){
        super(iswhite);
    }

    /**
     * Implements abstract method of the same name in Piece according to the move set of a bishop
     *  <p>CASES: Cases where bishop move is valid
     *  <ul>
     *  <li> CASE 1: Bishop moves vertically increasing, horizontally increasing
     *  <li> CASE 2: Bishop moves vertically decreasing, horizontally increasing
     *  <li> CASE 3: Bishop moves vertically decreasing, horizontally decreasing
     *  <li> CASE 3: Bishop moves vertically increasing, horizontally increasing
     *  </ul>
     * @param src   Source space from which the piece will move
     * @return  ArrayList of Coordinates of all possible paths for a bishop at the source
     */
    public ArrayList<Coordinate> returnCoordinatePath(Space src){
        ArrayList<Coordinate> path = new ArrayList<>();
        int file = src.getPosition().getFile();
        int rank = src.getPosition().getRank();

        // return all diagonal elements as long as it falls within the board
        // vertically increasing, horizontally increasing
        for(int i = 1; i < 8; ++i){
            for(int j = 1; j < 8; ++j){
                Coordinate coord = new Coordinate(file+j, rank+i);
                if(rank+i > 8 || file+j > 8 || (abs(rank - coord.getRank()) != abs(file - coord.getFile())))
                    continue;
                path.add(coord);
            }
        }

        // vertically decreasing, horizontally increasing
        for(int i = 7; i >= 1; --i){
            for(int j = 1; j < 8; ++j){
                Coordinate coord = new Coordinate(file+j, rank-i);
                if(rank-i < 1 || file+j > 8 || (abs(rank - coord.getRank()) != abs(file - coord.getFile())))
                    continue;
                path.add(coord);
            }
        }

        // vertically decreasing, horizontally decreasing
        for(int i = 7; i >= 1; --i){
            for(int j = 7; j >= 1; --j){
                Coordinate coord = new Coordinate(file-j, rank-i);
                if(rank-i < 1 || file-j < 1 || (abs(rank - coord.getRank()) != abs(file - coord.getFile())))
                    continue;
                path.add(coord);
            }
        }

        // vertically increasing, horizontally increasing
        for(int i = 1; i < 8; ++i){
            for(int j = 7; j >= 1; --j){
                Coordinate coord = new Coordinate(file-j, rank+i);
                if(rank+i > 8 || file-j < 1 || (abs(rank - coord.getRank()) != abs(file - coord.getFile())))
                    continue;
                path.add(coord);
            }
        }

        return path;
    }

    /**
     * Implements toString override in Piece where type is represented as "B"
     * @return  "wB" for white bishop or "bB" for black bishop.
     */
    public String toString(){
        if(isWhite()){
            return "wB";
        }else{
            return "bB";
        }
    }
}
