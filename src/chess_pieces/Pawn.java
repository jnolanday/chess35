/**
 * @author Group 35: Jarisha Olanday (jmo185) & Leo Scarano (ls949)
 */

package chess_pieces;
import chess_board.Coordinate;
import chess_board.Space;

import java.util.ArrayList;

/**
 * Represents a pawn chess piece. There are 8 total pawns per player in the game of chess
 *  @author Group 35: Jarisha Olanday (jmo185) & Leo Scarano (ls949)
 */
public class Pawn extends Piece{

    /**
     * Contains whether or not the pawn performed a double-step for the first move
     */
    private boolean doubleStep;

    /**
     * Creates a new Pawn with the specified color using super constructor.
     * <p>The doubleStep field is initialized to false
     * @param iswhite boolean for whether or not the piece is white or black (true if white, false if black)
     */
    public Pawn(boolean iswhite){
        super(iswhite);
        doubleStep = false;
    }

    /**
     * Implements abstract method of the same name in Piece according to the move set of a pawn
     *  <p>CASES: Cases where pawn move is valid
     *  <ul>
     *  <li> CASE 1: Pawn moves forward two spaces
     *  <li> CASE 2: Pawn moves forward ong spaces
     *  <li> CASE 3: Pawn moves diagonal 1 space to capture piece at destination
     *  <li> CASE 4: En Passant (covered at chess-level)
     *  </ul>
     *
     * @param src   Source space from which the piece will move
     * @return  ArrayList of Coordinates of all possible paths for a pawn at the source
     */
    public ArrayList<Coordinate> returnCoordinatePath(Space src){
        ArrayList<Coordinate> path = new ArrayList<>();
        boolean white = src.getPiece().isWhite();
        int file = src.getPosition().getFile();
        int rank = src.getPosition().getRank();

        // Pawn moves two spaces ahead
        if(white && rank == 2)
            path.add(new Coordinate(file, rank+2));
        else if(!white && rank == 7)
            path.add(new Coordinate(file, rank-2));

        // Pawn moves one space ahead
        if(white)
            path.add(new Coordinate(file, rank+1));
        else
            path.add(new Coordinate(file, rank-1));

        // white Pawn captures
        if(white && file == 8)
            path.add(new Coordinate(file-1, rank+1));
        else if(white && file == 1)
            path.add(new Coordinate(file+1,rank+1));
        else if(white && file > 1 && file < 8) {
            path.add(new Coordinate(file+1, rank+1));
            path.add(new Coordinate(file-1, rank+1));
        }

        // black pawn captures
        if(!white && file == 8)
            path.add(new Coordinate(file-1, rank-1));
        else if(!white && file == 1)
            path.add(new Coordinate(file+1, rank-1));
        else if(!white && file > 1 && file < 8){
            path.add(new Coordinate(file+1, rank-1));
            path.add(new Coordinate(file-1, rank-1));
        }

        return path;
    }

    /**
     * Returns doubleStep field of Pawn
     * @return  boolean doubleStep field of Pawn
     */
    public boolean getDoubleStep(){
        return doubleStep;
    }

    /**
     * Sets the doubleStep field to given value
     * <p>Used for undoing a double-step pawn move
     * @param val   new value to set doubleStep field
     */
    public void setDoubleStep(boolean val){
        doubleStep = val;
    }

    /**
     * Changes the doubleStep field according to the given move (source and destination coordinate pair)
     * <p>Assumes the given source and destination coordinate pair is valid
     * <p>Sets doubleStep to true if the first move is moving two spaces up.
     * Otherwise sets doubleStep to false if it performed any other valid move for its first move or subsequent moves.
     * <p>Used in Board movePiece  method
     * @param src   Initial location of the pawn
     * @param dest  Location to which the pawn was moved
     */
    public void updateStep(Coordinate src, Coordinate dest){
        if((isWhite() && src.getRank() == 2 && dest.getFile() == src.getFile() && dest.getRank() - src.getRank() == 2) ||
            (!isWhite() && src.getRank() == 7 && dest.getFile() == src.getFile() && src.getRank() - dest.getRank() == 2)){
            doubleStep = true;
        }else{
            doubleStep = false;
        }
    }


    /**
     * Implements toString override in Piece where type is represented as "p"
     * @return  "wp" for white pawn or "bp" for black pawn.
     */
    public String toString(){
        if(isWhite()){
            return "wp";
        }else{
            return "bp";
        }
    }
}
