package chess_pieces;
import chess_board.Coordinate;
import chess_board.Space;

import java.lang.reflect.Array;
import java.util.ArrayList;

import static java.lang.Math.abs;


/**
 * Represents a queen chess piece. There is 1 queen per player in the game of chess
 *  @author Group 35: Jarisha Olanday (jmo185) & Leo Scarano (ls949)
 */
public class Queen extends Piece {

    /**
     * Creates a new Queen with the specified color using the super constructor
     * @param iswhite boolean for whether or not the piece is white or black (true if white, false if black)
     */
    public Queen(boolean iswhite){
        super(iswhite);
    }

    /**
     * Implements abstract method of the same name in Piece according to the move set of a Queen
     *  <p>CASES: Cases where Queen move is valid
     *  <ul>
     *  <li> CASE 1: Queen moves vertically increasing
     *  <li> CASE 2: Queen moves horizontally increasing
     *  <li> CASE 3: Queen moves vertically decreasing
     *  <li> CASE 4: Queen moves horizontally decreasing
     *  <li> CASE 5: Queen moves vertically increasing, horizontally increasing
     *  <li> CASE 6: Queen moves vertically decreasing, horizontally increasing
     *  <li> CASE 7: Queen moves vertically decreasing, horizontally decreasing
     *  <li> CASE 8: Queen moves vertically increasing, horizontally increasing
     *  </ul>
     * @param src   Source space from which the piece will move
     * @return  ArrayList of Coordinates of all possible paths for a Queen at the source
     */
    public ArrayList<Coordinate> returnCoordinatePath(Space src){
        ArrayList<Coordinate> path = new ArrayList<>();
        int file = src.getPosition().getFile();
        int rank = src.getPosition().getRank();

        // Rook implementation
        // Increase vertically
        for(int i = 1; i + rank <= 8; ++i){
            path.add(new Coordinate(file, rank+i));
        }
        // Increase horizontally
        for(int j = 1; j + file <= 8; ++j){
            path.add(new Coordinate(file+j, rank));
        }
        // decrease vertically
        for(int i = 1; rank - i >= 1; ++i){
            path.add(new Coordinate(file, rank-i));
        }
        // decrease horizontally
        for(int j = 1; file - j >= 1; ++j){
            path.add(new Coordinate(file-j, rank));
        }

        // Bishop implementation
        // return all diagonal elements as long as it falls within the board
        // vertically increasing, horizontally increasing
        for(int i = 1; i < 8; ++i){
            for(int j = 1; j < 8; ++j){
                Coordinate coord = new Coordinate(file+j, rank+i);
                if(rank+i > 8 || file+j > 8 || (abs(rank - coord.getRank()) != abs(file - coord.getFile())))
                    continue;
                path.add(coord);
            }
        }

        // vertically decreasing, horizontally increasing
        for(int i = 7; i >= 1; --i){
            for(int j = 1; j < 8; ++j){
                Coordinate coord = new Coordinate(file+j, rank-i);
                if(rank-i < 1 || file+j > 8 || (abs(rank - coord.getRank()) != abs(file - coord.getFile())))
                    continue;
                path.add(coord);
            }
        }

        // vertically decreasing, horizontally decreasing
        for(int i = 7; i >= 1; --i){
            for(int j = 7; j >= 1; --j){
                Coordinate coord = new Coordinate(file-j, rank-i);
                if(rank-i < 1 || file-j < 1 || (abs(rank - coord.getRank()) != abs(file - coord.getFile())))
                    continue;
                path.add(coord);
            }
        }

        // vertically increasing, horizontally increasing
        for(int i = 1; i < 8; ++i){
            for(int j = 7; j >= 1; --j){
                Coordinate coord = new Coordinate(file-j, rank+i);
                if(rank+i > 8 || file-j < 1 || (abs(rank - coord.getRank()) != abs(file - coord.getFile())))
                    continue;
                path.add(coord);
            }
        }

        return path;
    }

    /**
     * Implements toString override in Piece where type is represented as "Q"
     * @return  "wQ" for white queen or "bQ" for black queen.
     */
    public String toString(){
        if(isWhite()){
            return "wQ";
        }else{
            return "bQ";
        }
    }
}
