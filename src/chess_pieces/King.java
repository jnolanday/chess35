package chess_pieces;
import chess.Chess;
import chess_board.Board;
import chess_board.Coordinate;
import chess_board.Space;

import java.util.ArrayList;

import static java.lang.Math.abs;

/**
 * Represents a king chess piece. There is 1 king per player in the game of chess
 *  @author Group 35: Jarisha Olanday (jmo185) & Leo Scarano (ls949)
 */
public class King extends Piece{
    private boolean iswhite;

    /**
     * Creates a new King with the specified color using super constructor
     * @param iswhite boolean for whether or not the piece is white or black (true if white, false if black)
     */
    public King(boolean iswhite){
        super(iswhite);
    }

    /**
     * Implements abstract method of the same name in Piece according to the move set of a king
     *  <p> A King can move 1 space in any direction     *
     * @param src   Source space from which the piece will move
     * @return  ArrayList of Coordinates of all possible paths for a king at the source
     */
    public ArrayList<Coordinate> returnCoordinatePath(Space src){
        ArrayList<Coordinate> path = new ArrayList<>();
        int file = src.getPosition().getFile();
        int rank = src.getPosition().getRank();

        for(int i = -1; i <= 1; ++i){
            for(int j = -1; j <= 1; ++j){
                // ignore the coordinate of the king and the coordinates that go off of the board
                if(j == i || rank + i > 8 || rank + i < 1 || file + j > 8 || file + j < 1)
                    continue;
                path.add(new Coordinate(file+j, rank+i));
            }
        }

        return path;
    }

    /**
     * Implements toString override in Piece where type is represented as "K"
     * @return  "wK" for white king or "bK" for black king.
     */
    public String toString(){
        if(isWhite()){
            return "wK";
        }else{
            return "bK";
        }
    }
}
