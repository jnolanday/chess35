

package chess_pieces;
import chess_board.Coordinate;
import chess_board.Space;

import java.util.ArrayList;
/**
 * Piece is the abstract base class for all chess pieces: Pawn, Knight, Rook, Bishop, Queen, and King.
 * A Piece object contains information on the color of the piece and contains a method to validate a move from one
 * space on the chess board to another.
 * <p>
 * Instances of Piece are members of the Space class of which the Board class consists.
 * <p>
 * There are a total of 32 Piece instances for the Chess game: 16 white Pieces and 16 black Pieces.
 * @author Group 35: Jarisha Olanday (jmo185) & Leo Scarano (ls949)
 */
public abstract class Piece {
    /**
     * True if the Piece is white, false if it is black
     */
    private boolean iswhite;

    /**
     * Creates a new Piece object with the specified color
     * @param color boolean for whether or not the piece is white or black
     */
    public Piece(boolean color){
        this.iswhite = color;
    }

    /**
     * Generates an ArrayList of Coordinates of all possible paths the piece can make
     * from the given space according to the type of piece and how it moves
     * @param src   Source space from which the piece will move
     * @return  ArrayList of Coordinates of all possible paths
     */
    public abstract ArrayList<Coordinate> returnCoordinatePath(Space src);

    /**
     * Override of toString to print color and type of the piece as
     * shown in the ascii art example of the chessboard
     * Example: A white Queen's toString() returns "wp"
     * @return String in the format of ColorType as so [w|b][p|R|N|B|Q|K]
     */
    public abstract String toString();

    /**
     * Returns the iswhite member of the Piece
     * @return  the iswhite member of the Piece
     */
    public boolean isWhite(){
        return this.iswhite;
    }


}
