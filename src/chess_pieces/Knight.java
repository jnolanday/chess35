package chess_pieces;
import chess_board.Coordinate;
import chess_board.Space;

import java.util.ArrayList;

import static java.lang.Math.abs;

/**
 * Represents a knight chess piece. There are 2 knights per player in the game of chess
 *  @author Group 35: Jarisha Olanday (jmo185) & Leo Scarano (ls949)
 */
public class Knight extends Piece {

    /**
     * Creates a new Knight with the specified color using super constructor
     * @param iswhite boolean for whether or not the piece is white or black (true if white, false if black)
     */
    public Knight(boolean iswhite){
        super(iswhite);
    }

    /**
     * Implements abstract method of the same name in Piece according to the move set of a knight
     *  <p>CASES: Cases where knight move is valid
     *  <ul>
     *  <li> CASE 1: Knight moves horizontally twice and vertically once
     *  <li> CASE 2: Knight moves vertically twice and horizontally once
     *  </ul>
     * @param src   Source space from which the piece will move
     * @return  ArrayList of Coordinates of all possible paths for a knight at the source
     */
    public ArrayList<Coordinate> returnCoordinatePath(Space src){
        int file = src.getPosition().getFile();
        int rank = src.getPosition().getRank();
        ArrayList<Coordinate> path = new ArrayList<>();

        // return possible L shape destination coordinates as long as it falls within the board
        if(rank+2 <= 8 && file+1 <= 8)
            path.add(new Coordinate(file+1, rank+2));
        if(rank+1 <= 8 && file+2 <= 8)
            path.add(new Coordinate(file+2, rank+1));
        if(rank-1 >= 1 && file+2 <= 8)
            path.add(new Coordinate(file+2, rank-1));
        if(rank-2 >= 1 && file+1 <= 8)
            path.add(new Coordinate(file+1, rank-2));
        if(rank-2 >= 1 && file-1 >= 1)
            path.add(new Coordinate(file-1, rank-2));
        if(rank-1 >= 1 && file-2 >= 2)
            path.add(new Coordinate(file-2, rank-1));
        if(rank+1 <= 8 && file - 2 >= 1)
            path.add(new Coordinate(file-2, rank+1));
        if(rank+2 <= 8 && file-1 >= 1)
            path.add(new Coordinate(file-1, rank+2));

            return path;
    }
    /**
     * Implements toString override in Piece where type is represented as "N"
     * @return  "wN" for white knight or "bN" for black knight.
     */
    public String toString(){
        if(isWhite()){
            return "wN";
        }else{
            return "bN";
        }
    }
}
