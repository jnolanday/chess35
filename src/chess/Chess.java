package chess;
import chess_board.*;
import chess_pieces.*;
import java.util.*;
import java.util.regex.*;

import static java.lang.Math.abs;

public class Chess {

    public static Board chessBoard = new Board();
    public static Status status;
    public static Player player;
    public static int numOfWhiteMoves = 0;
    public static int numOfBlackMoves = 0;
    public static int enPassantAvail = 0;
    public static boolean enPassantComplete = false;
    public static int counter = 0;

    enum Player{
        WHITE,BLACK
    }

    enum Status{
        ONGOING, DRAWPENDING
    }

    public static void printTurn(){
        System.out.println(player.name() + "'S TURN:");
    }

    public static void switchTurn(){
        if(player.equals(Player.WHITE)){
            player = Player.BLACK;
        }else{
            player = Player.WHITE;
        }

    }

    public static boolean checkWhiteCheckmate(Space whiteKingCoord){
        boolean flag = true;
        for(int i = 1; i <= 8; ++i){
            for(int j = 1; j <= 8; ++j){
                Coordinate current = new Coordinate(j, i);
                Space currentSpace = chessBoard.getSpace(current);
                if(!currentSpace.isEmpty){
                    if(!currentSpace.isWhite){
                        ArrayList<Coordinate> path = chessBoard.getPiece(current).returnCoordinatePath(currentSpace);
                        for(Coordinate c : path){
                            Space temp = chessBoard.getSpace(c);
                            // simulate move of a particular piece to the possible path, then check for check
                            if(checkIsValidMove(currentSpace, temp)) {
                                // if king, must pass in king's position accordingly
                                if (chessBoard.getPiece(current) instanceof King) {
                                    // move piece to path
                                    chessBoard.movePiece(current, c);
                                    if (!checkWhiteCheck(temp)) {
                                        flag = false;
                                    }
                                } else {
                                    // move piece to path
                                    chessBoard.movePiece(current, c);
                                    if (!checkWhiteCheck(whiteKingCoord)) {
                                        flag = false;
                                    }
                                }
                                // undo move
                                chessBoard.undoMove(c, current, temp);
                            }
                            if(flag == false)
                                return false;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static boolean checkBlackCheckmate(Space blackKingCoord){
        boolean flag = true;
        for(int i = 1; i <= 8; ++i){
            for(int j = 1; j <= 8; ++j){
                Coordinate current = new Coordinate(j, i);
                Space currentSpace = chessBoard.getSpace(current);
                if(!currentSpace.isEmpty){
                    if(currentSpace.isWhite){
                        ArrayList<Coordinate> path = chessBoard.getPiece(current).returnCoordinatePath(currentSpace);
                        for(Coordinate c : path){
                            if(counter == 1)
                                System.exit(0);
                            Space temp = chessBoard.getSpace(c);
                            // simulate move of a particular piece to the possible path, then check for check
                            if(checkIsValidMove(currentSpace, temp)) {
                                // if king, must pass in king's position accordingly
                                if (chessBoard.getPiece(current) instanceof King) {
                                    // move piece to path
                                    chessBoard.movePiece(current, c);
                                    if (!checkBlackCheck(temp)) {
                                        flag = false;
                                    }
                                } else {
                                    // move piece to path
                                    chessBoard.movePiece(current, c);
                                    if (!checkBlackCheck(blackKingCoord)) {
                                        flag = false;
                                    }
                                }
                                // undo move
                                chessBoard.undoMove(c, current, temp);
                                counter = 1;
                            }
                            if(flag == false)
                                return false;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static boolean checkIsValidMove(Space srcSpace, Space destSpace){
        int srcFile = srcSpace.getPosition().getFile();
        int srcRank = srcSpace.getPosition().getRank();
        int destFile = destSpace.getPosition().getFile();
        int destRank = destSpace.getPosition().getRank();

        // Check for En Passant
        if(srcSpace.getPiece() instanceof Pawn){
            /*
             * Case 1: Pawn performs double-step and sets up en passant for opponent
             */
            if(abs(destRank - srcRank) == 2 && srcFile == destFile) {
                Space leftCapturingSpace = new Space(destSpace.isWhite, destSpace.getPosition());
                Space rightCapturingSpace = new Space(destSpace.isWhite, destSpace.getPosition());

                if (destFile - 1 > 0) {
                    leftCapturingSpace = chessBoard.getSpace(new Coordinate(destFile - 1, destRank));
                }
                if (destFile + 1 < 8) {
                    rightCapturingSpace = chessBoard.getSpace(new Coordinate(destFile + 1, destRank));
                }

                if ((srcSpace.getPiece().isWhite() && srcRank == 2
                        && ((leftCapturingSpace.getPiece() instanceof Pawn && !leftCapturingSpace.getPiece().isWhite())
                        || (rightCapturingSpace.getPiece() instanceof Pawn && !rightCapturingSpace.getPiece().isWhite()))) ||
                        (!srcSpace.getPiece().isWhite() && srcRank == 7
                                && ((leftCapturingSpace.getPiece() instanceof Pawn && leftCapturingSpace.getPiece().isWhite())
                                || (rightCapturingSpace.getPiece() instanceof Pawn && rightCapturingSpace.getPiece().isWhite())))) {
                    enPassantAvail = 1;
                    return true;
                }
            }

            /*
             * Case 2: Pawn performs en passant capture after setup by opponent
             */
            if(enPassantAvail == 2 && (abs(destRank - srcRank) == 1 && abs(destFile-srcFile) == 1)){
                Space capturedSpace = chessBoard.getSpace(destSpace.getPosition());
                if(srcSpace.getPiece().isWhite() && srcRank == 5){
                    capturedSpace = chessBoard.getSpace(new Coordinate(destFile,destRank - 1));
                }else if(!srcSpace.getPiece().isWhite() && srcRank == 4) {
                    capturedSpace = chessBoard.getSpace(new Coordinate(destFile, destRank + 1));
                }
                Piece capturedPiece = capturedSpace.getPiece();

                if(capturedPiece instanceof Pawn && ((Pawn) capturedPiece).getDoubleStep()){
                    chessBoard.board[capturedSpace.getPosition().getFile() - 1][capturedSpace.getPosition().getRank() - 1].removePiece();
                    enPassantComplete = true;
                    return true;
                }
            }
        }

        // Castling goes here

        // Cover pawn case where you may not be able to capture
        // Dest is in the same file and an opposing player's piece occupies the dest space, do not allow
        // OR Pawn is attempting to move diagonally in a capture but destination is empty
        if((!destSpace.isEmpty && srcSpace.getPiece() instanceof Pawn && srcFile == destSpace.getPosition().getFile()) ||
                destSpace.isEmpty && srcSpace.getPiece() instanceof Pawn && (abs(destRank - srcRank) == 1 && abs(destFile-srcFile) == 1))
            return false;

        /* Iterate through paths of the piece, cover three cases
            1. If the space at iteration i is only one space away, allow it every time (failed pawn capture is already covered)
            2. If the space at iteration i is more than one space away, is between the src and dest, and contains a piece, do not allow
         */
        Coordinate destCoordInPath = null;
        Space currSpace = null;
        int currSpaceRank;
        int currSpaceFile;
        ArrayList<Coordinate> path = srcSpace.getPiece().returnCoordinatePath(srcSpace);
        //System.out.println("path: " + path.toString());
        for(Coordinate c : path){
            // If piece is one space away accept it
            if(((abs(destRank - srcRank) == 0 && abs(destFile-srcFile) == 1)
                    || (abs(destRank - srcRank) == 1 && abs(destFile-srcFile) == 0)
                    || (abs(destRank - srcRank) == 1 && abs(destFile-srcFile) == 1))
                    && (destFile == c.getFile() && destRank == c.getRank()))
                return true;

            // Save dest coord, if this is never true, dest coord is not in path and the method returns false
            if(destFile == c.getFile() && destRank == c.getRank())
                destCoordInPath = c;

            currSpace = chessBoard.getSpace(c);
            currSpaceRank = currSpace.getPosition().getRank();
            currSpaceFile = currSpace.getPosition().getFile();

            // Determine if there is a piece between the src and dest space, if there is, move is invalid
            if(!currSpace.isEmpty){
                // CASE 1: path piece to the left of src piece and right of dest piece
                if(srcRank == destRank && destRank == currSpaceRank
                        && destFile < srcFile && currSpaceFile < srcFile
                        && destFile < currSpaceFile)
                    return false;
                // CASE 2: path piece to the right of src piece and left of dest piece
                if(srcRank == destRank && destRank == currSpaceRank
                        && destFile > srcFile && currSpaceFile > srcFile
                        && destFile > currSpaceFile)
                    return false;
                // CASE 3: path piece below src piece and above dest piece
                if(srcFile == destFile && destFile == currSpaceFile
                        && destRank < srcRank && currSpaceRank < srcRank
                        && destRank < currSpaceRank)
                    return false;
                // CASE 4: path piece above src piece and below destPiece
                if(srcFile == destFile && destFile == currSpaceFile
                        && destRank > srcRank && currSpaceRank > srcRank
                        && destRank > currSpaceRank)
                    return false;
                // CASE 5: path piece is in between the dest and src piece on the upper-leftmost diagonal
                if(abs(destFile - srcFile) == abs(destRank - srcRank)
                        && abs(currSpaceFile - srcFile) == abs(currSpaceRank - srcRank)
                        && destFile < srcFile && destRank > srcRank
                        && currSpaceFile < srcFile && currSpaceRank > srcRank
                        && currSpaceRank < destRank)
                    return false;
                // CASE 6: path piece is in between the dest and src piece on the lower right-most diagonal
                if(abs(destFile - srcFile) == abs(destRank - srcRank)
                        && abs(currSpaceFile - srcFile) == abs(currSpaceRank - srcRank)
                        && destFile > srcFile && destRank < srcRank
                        && currSpaceFile > srcFile && currSpaceRank < srcRank
                        && currSpaceRank > destRank)
                    return false;
                // CASE 7 path piece is in between the dest and src piece on the upper right-most diagonal
                if(abs(destFile - srcFile) == abs(destRank - srcRank)
                        && abs(currSpaceFile - srcFile) == abs(currSpaceRank - srcRank)
                        && destFile > srcFile && destRank > srcRank
                        && currSpaceFile > srcFile && currSpaceRank > srcRank
                        && currSpaceRank < destRank)
                    return false;
                // CASE 8: path piece is in between the dest and src piece on the lower left-most diagonal
                if(abs(destFile - srcFile) == abs(destRank - srcRank)
                        && abs(currSpaceFile - srcFile) == abs(currSpaceRank - srcRank)
                        && destFile < srcFile && destRank < srcRank
                        && currSpaceFile < srcFile && currSpaceRank < srcRank
                        && currSpaceRank > destRank)
                    return false;
            }
        }
        if(destCoordInPath == null)
            return false;
        else
            return true;
    }

    public static boolean checkWhiteCheck(Space whiteKingCoord){
        for(int i = 1; i <= 8; ++i){
            for(int j = 1; j <= 8; ++j){
                Coordinate current = new Coordinate(j,i);
                if(!chessBoard.getSpace(current).isEmpty) {
                    if (!chessBoard.getPiece(current).isWhite()) {
                        Space currentSpace = chessBoard.getSpace(current);
                        if (checkIsValidMove(currentSpace, whiteKingCoord)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static boolean checkBlackCheck(Space blackKingCoord){
        for(int i = 1; i <= 8; ++i){
            for(int j = 1; j <= 8; ++j){
                Coordinate current = new Coordinate(j,i);
                if(!chessBoard.getSpace(current).isEmpty) {
                    if (chessBoard.getPiece(current).isWhite()) {
                        Space currentSpace = chessBoard.getSpace(current);
                        if (checkIsValidMove(currentSpace, blackKingCoord)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static int processMove(String move) {
        //Check for resign, update status if true
        if (move.equals("resign") && player.equals(Player.WHITE)) {
            System.out.println("Black wins");
            return 2;
        } else if (move.equals("resign") && player.equals(Player.BLACK)) {
            System.out.println("White wins");
            return 2;
        }

        //Check for draw accept, change status back to ONGOING if not accepted
        if(move.equals("draw") && status.equals(Status.DRAWPENDING)){
            System.out.println("Draw");
            return 1;
        }else if(!move.equals("draw") && status.equals(Status.DRAWPENDING)){
            status = Status.ONGOING;
        }

        //First parse input string
        Matcher matcher = Pattern.compile("([a-h][1-8])\\s([a-h][1-8])\\s?(R|N|B|Q)?\\s?(draw\\?)?").matcher(move);

        String src, dst, drawOffer, promotionType;

        if(matcher.matches()){
            src = matcher.group(1);
            dst = matcher.group(2);
            promotionType = matcher.group(3);
            drawOffer = matcher.group(4);

        }else{
            System.out.println("Invalid input, try again");
            return -1;
        }

        //Check for draw offer, update status if true
        if(drawOffer != null && !drawOffer.isEmpty()){
            status = Status.DRAWPENDING;
        }

        //Get spaces, pieces, and coordinates from
        Coordinate c_src = new Coordinate(src);
        Coordinate c_dst = new Coordinate(dst);
        Piece p_src = chessBoard.getPiece(c_src);
        Piece p_dst = chessBoard.getPiece(c_dst);
        Space srcSpace = chessBoard.getSpace(c_src);
        Space destSpace = chessBoard.getSpace(c_dst);

        //Check if there is a piece at the src (check if space is empty)
        if(srcSpace.isEmpty){
            System.out.println("Illegal move, try again");
            return -1;
        }
        //Check if piece at src space matches the player color
        if(player.equals(Player.WHITE) && !p_src.isWhite()){
            System.out.println("Illegal move, try again");
            return -1;
        }else if(player.equals(Player.BLACK) && p_src.isWhite()){
            System.out.println("Illegal move, try again");
            return -1;
        }

        //If destination space is not empty, check if piece at destination is an opposing color for valid capture
        if(!destSpace.isEmpty){
            if(player.equals(Player.WHITE) && p_dst.isWhite()){
                System.out.println("Illegal move, try again");
                return -1;
            }else if (player.equals(Player.BLACK) && !p_dst.isWhite()){
                System.out.println("Illegal move, try again");
                return -1;
            }
        }


        //Check if move is a valid for the piece
        if(checkIsValidMove(srcSpace, destSpace)) {

            // Move is valid, update board
            chessBoard.movePiece(c_src, c_dst);

            boolean moveking = false;

            // if player moves king, update king coordinate
            if (destSpace.getPiece() instanceof King && destSpace.getPiece().isWhite()) {
                chessBoard.whiteKingCoord = chessBoard.getSpace(c_dst);
                moveking = true;
            }
            else if(destSpace.getPiece() instanceof King && !destSpace.getPiece().isWhite()){
                chessBoard.blackKingCoord = chessBoard.getSpace(c_dst);
                moveking = true;
            }


            // If player's king is  in check after move, undo move
            if(player.equals(Player.BLACK) && checkBlackCheck(chessBoard.blackKingCoord)) {
                // must undo move
                chessBoard.movePiece(c_dst,c_src);
                if(!destSpace.isEmpty) {
                    chessBoard.board[c_dst.getRank() - 1][c_dst.getFile() - 1].addPiece(destSpace.getPiece());
                }
                if(moveking)
                    chessBoard.blackKingCoord = srcSpace;
                System.out.println("Illegal move, try again");
                return -1;
            }else if(player.equals(Player.WHITE) && checkWhiteCheck(chessBoard.whiteKingCoord)){
                // must undo move
                chessBoard.movePiece(c_dst,c_src);
                if(!destSpace.isEmpty) {
                    chessBoard.board[c_dst.getRank() - 1][c_dst.getFile() - 1].addPiece(destSpace.getPiece());
                }
                if(moveking)
                    chessBoard.blackKingCoord = srcSpace;
                System.out.println("Illegal move, try again");
                return -1;
            }

            // If player places the opponents king in check, display check
            if(player.equals(Player.BLACK) && checkWhiteCheck(chessBoard.whiteKingCoord)) {
                if(checkWhiteCheckmate(chessBoard.whiteKingCoord)){
                    System.out.print("Checkmate");
                    System.out.print("Black wins");
                    return 2;
                }
                System.out.println("Check");
            }
            else if(player.equals(Player.WHITE) && checkBlackCheck(chessBoard.blackKingCoord)) {
                if(checkBlackCheckmate(chessBoard.blackKingCoord)){
                    System.out.print("Checkmate");
                    System.out.print("White wins");
                    return 2;
                }
                System.out.println("Check");
            }

            //Check for promotion
            if(player.equals(Player.WHITE) && p_src instanceof Pawn && c_dst.getRank() == 8){
                chessBoard.promote(c_dst,true,promotionType);
            }else if(player.equals(Player.BLACK) && p_src instanceof Pawn && c_dst.getRank() == 1){
                chessBoard.promote(c_dst,false,promotionType);
            }

            if(enPassantAvail == 1){
                enPassantAvail = 2;
                //System.out.println("En passant available");
            }else if(enPassantAvail == 2 && enPassantComplete){
                enPassantAvail = 0;
                enPassantComplete = false;
                //System.out.println("En passant successful, resetting");
            }else if(enPassantAvail == 2 && !enPassantComplete){
                enPassantAvail = 0;
                enPassantComplete = false;
                //System.out.println("Right to en passant lost, resetting");
            }
            // update move count for stalemate, checkmate and check conditions
            if(destSpace.getPiece().isWhite())
                numOfWhiteMoves++;
            else{
                numOfBlackMoves++;
            }

        }else{
            System.out.println("Illegal move, try again");
            return -1;
        }

        return 0;
    }


    public static void main (String[] args){
        System.out.println("2-PLAYER CHESS GAME START");
        status = Status.ONGOING;
        player = Player.WHITE;
        Scanner scn = new Scanner(System.in);

        while(status.equals(Status.ONGOING) || status.equals(Status.DRAWPENDING)){
            chessBoard.print();
            printTurn();
            String move = scn.nextLine();

            int retval = processMove(move);
            if(retval == 0){
                switchTurn();
            }
            // Draw & resign & checkmate case
            else if(retval == 1 || retval == 2){
                break;
            }
        }
    }


}
